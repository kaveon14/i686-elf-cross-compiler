[Link to Docker Hub](https://hub.docker.com/repository/docker/kaveon14/i686-elf-cross-compiler).

This is a GCC Cross Compiler targeted for **i686-elf**. This docker image was built by following the [OS Dev GCC Cross-Compiler Tutorial](https://wiki.osdev.org/GCC_Cross-Compiler). This cross compiler image can be used like so:

```
 docker run --rm -v "$PWD":/usr/src/myapp -w /usr/src/myapp kaveon14/i686-elf-cross-compiler:latest /opt/cross/bin/i686-elf-gcc --version
```

It can also be used in a Dockerfile like so:

```
FROM kaveon14/i686-elf-cross-compiler:latest
RUN /opt/cross/bin/i686-elf-gcc --version
```
