FROM alpine:3.7
RUN apk add --no-cache build-base && apk add --no-cache git

ENV PREFIX $HOME/opt/cross
ENV TARGET i686-elf
ENV PATH $PREFIX/bin:$PATH

# Install GCC Cross Compiler Dependencies
RUN apk add --no-cache gmp-dev \
	&& apk add --no-cache mpfr-dev \
	&& apk add --no-cache mpc1-dev \
	&& apk add --no-cache flex \
	&& apk add --no-cache texinfo \
	&& apk add --no-cache bison

# Make the binutils
WORKDIR $HOME/cross_compiler
RUN git clone --depth 1 git://sourceware.org/git/binutils-gdb.git \
    && mkdir build-binutils \
    && cd build-binutils \
    && ../binutils-gdb/configure --target=$TARGET --prefix="$PREFIX" --with-sysroot --disable-nls --disable-werror \
	&& make \
	&& make install \
	&& cd .. \
	&& rm -rf binutils-gdb

# Make the Cross Compiler
RUN git clone --depth 1 git://gcc.gnu.org/git/gcc.git \
	&& mkdir build-gcc \
	&& cd build-gcc \
	&& ../gcc/configure --target=$TARGET --prefix="$PREFIX" --disable-nls --enable-languages=c,c++ --without-headers \
	&& make all-gcc \
	&& make all-target-libgcc \
	&& make install-gcc \
	&& make install-target-libgcc \
	&& cd .. \
	&& rm -rf gcc
